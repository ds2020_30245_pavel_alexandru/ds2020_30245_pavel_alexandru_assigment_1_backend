package backend.controllers;

import backend.dtos.UserDTO;
import backend.services.UserService;
import ch.qos.logback.classic.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) { this.userService = userService; }

    @GetMapping(value = "/{username}/{password}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("username") String username,@PathVariable("password") String password) {

        UserDTO dto = userService.findUser(username,password);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertUser(@Valid @RequestBody UserDTO user) {
        UUID userID = userService.insert(user);
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteUser(@PathVariable("id") UUID id) {

        userService.delete(id);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") UUID accountId,@Valid @RequestBody UserDTO user) {

        userService.update(accountId,user.getUserName(),user.getPassword());
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @DeleteMapping(value = "/byAccountId/{id}")
    public ResponseEntity<UUID> deleteByAccountId(@PathVariable("id") UUID id) {

        userService.deleteByAccountId(id);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }
}
