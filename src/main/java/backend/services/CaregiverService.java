package backend.services;

import backend.dtos.CaregiverDTO;
import backend.dtos.PatientDTO;
import backend.dtos.builders.CaregiverBuilder;
import backend.dtos.builders.PatientBuilder;
import backend.entities.Caregiver;
import backend.entities.Patient;
import backend.entities.Users;
import backend.repositories.CaregiverRepo;

import backend.repositories.PatientRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepo caregiverRepo;
    private final PatientRepo patientRepo;

    @Autowired
    public CaregiverService(CaregiverRepo caregiverRepo,PatientRepo patientRepo) {this.caregiverRepo= caregiverRepo; this.patientRepo=patientRepo;}

    public CaregiverDTO findById(UUID id) {

        Optional<Caregiver> caregiver = caregiverRepo.findById(id);
        if (!caregiver.isPresent()) {
            LOGGER.error("Caregiver not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(caregiver.get());
    }

    public ArrayList<CaregiverDTO> findByName(String name) {

        ArrayList<Caregiver> caregiver = caregiverRepo.findByName(name);
        if ( caregiver.isEmpty()) {
            LOGGER.error("Caregivers not found.");
        }
        return (ArrayList<CaregiverDTO>) caregiver.stream().map(CaregiverBuilder::toCaregiverDTO).collect(Collectors.toList());
    }

    public ArrayList<CaregiverDTO> findByDob(String dob) {

        Date dob1= null;
        try {
            dob1 = new SimpleDateFormat("yyyy-MM-dd").parse(dob);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<Caregiver> caregiver = caregiverRepo.findByDob(dob1);
        if ( caregiver.isEmpty()) {
            LOGGER.error("Caregivers not found.");
        }
        return (ArrayList<CaregiverDTO>) caregiver.stream().map(CaregiverBuilder::toCaregiverDTO).collect(Collectors.toList());
    }

    public ArrayList<CaregiverDTO> findByGender(String gender) {

        ArrayList<Caregiver> caregiver = caregiverRepo.findByGender(gender);
        if ( caregiver.isEmpty()) {
            LOGGER.error("Caregivers not found.");
        }
        return (ArrayList<CaregiverDTO>) caregiver.stream().map(CaregiverBuilder::toCaregiverDTO).collect(Collectors.toList());
    }

    public ArrayList<CaregiverDTO> findByAddress(String address) {

        ArrayList<Caregiver> caregiver = caregiverRepo.findByAddress(address);
        if ( caregiver.isEmpty()) {
            LOGGER.error("Caregivers not found.");
        }
        return (ArrayList<CaregiverDTO>) caregiver.stream().map(CaregiverBuilder::toCaregiverDTO).collect(Collectors.toList());
    }

    public ArrayList<CaregiverDTO> findByPatients(String patients) {

        ArrayList<Caregiver> caregiver = caregiverRepo.findByPatients(patients);
        if ( caregiver.isEmpty()) {
            LOGGER.error("Caregivers not found.");
        }
        return (ArrayList<CaregiverDTO>) caregiver.stream().map(CaregiverBuilder::toCaregiverDTO).collect(Collectors.toList());
    }

    public ArrayList<CaregiverDTO> findAll()
    {
        ArrayList<Caregiver> caregivers = (ArrayList<Caregiver>) caregiverRepo.findAll();
        if ( caregivers.isEmpty()) {
            LOGGER.error("Caregivers not found.");
        }
        return (ArrayList<CaregiverDTO>) caregivers.stream().map(CaregiverBuilder::toCaregiverDTO).collect(Collectors.toList());
    }

    public UUID insert(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver.setPatients("");
        caregiver = caregiverRepo.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public boolean delete(UUID id)
    {
        caregiverRepo.deleteById(id);
        return true;
    }

    public boolean update(UUID id,CaregiverDTO caregiverDTO)
    {
        Optional<Caregiver> caregiver = caregiverRepo.findById(id);
        if (!caregiver.isPresent()) {
            LOGGER.error("Caregiver not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }

        Caregiver caregiver1 = caregiver.get();

        caregiver1.setName(caregiverDTO.getName());
        caregiver1.setDob(caregiverDTO.getDob());
        caregiver1.setGender(caregiverDTO.getGender());
        caregiver1.setAddress(caregiverDTO.getAddress());
        caregiver1.setPatients(caregiverDTO.getPatients());
        caregiverRepo.save(caregiver1);
        return true;
    }

    public ArrayList<PatientDTO> getPatients(UUID id)
    {
        Optional<Caregiver> caregiver = caregiverRepo.findById(id);
        if (!caregiver.isPresent()) {
            LOGGER.error("Caregiver not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }

        Caregiver caregiver1 = caregiver.get();

        String patients = caregiver1.getPatients();
        if ( patients.isEmpty()) {
            LOGGER.error("No patients.");
        }
        String[] spl = patients.split("/");

        ArrayList<Patient> patients1 = new ArrayList<>();
        for(String idPatient : spl)
        {
            try {
                Patient patient = patientRepo.findById(UUID.fromString(idPatient)).get();
                patients1.add(patient);
            }
            catch (Exception e)
            {
                LOGGER.debug("Patient not longer found on the system.");
                try {
                    removePatient(id, UUID.fromString(idPatient));
                }
                catch (Exception e1)
                {
                    LOGGER.debug("Ignore this nested exception.");
                }
            }
        }
        return (ArrayList<PatientDTO>) patients1.stream().map(PatientBuilder::toPatientDTO).collect(Collectors.toList());
    }

    public void addPatient(UUID id, UUID idPatient)
    {
        Optional<Caregiver> caregiver = caregiverRepo.findById(id);
        if (!caregiver.isPresent()) {
            LOGGER.error("Caregiver not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }
        Caregiver caregiver1 = caregiver.get();
        if(caregiver1.getPatients().isEmpty())
        {
            caregiver1.setPatients(idPatient.toString());
        }
        else
        {
            caregiver1.setPatients(caregiver1.getPatients()+"/"+idPatient.toString());
        }
        caregiverRepo.save(caregiver1);
    }

    public void removePatient(UUID id,UUID idPatient)
    {
        Optional<Caregiver> caregiver = caregiverRepo.findById(id);
        if (!caregiver.isPresent()) {
            LOGGER.error("Caregiver not found.");
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + id);
        }
        Caregiver caregiver1 = caregiver.get();
        String patients = caregiver1.getPatients();
        if ( patients.isEmpty()) {
            LOGGER.error("No patients.");
        }

        String[] spl = patients.split("/");

        for(String idPat : spl)
        {
            if(idPat.equals(idPatient.toString()))
            {
                caregiver1.setPatients(caregiver1.getPatients().replace(idPat+"/",""));
                caregiver1.setPatients(caregiver1.getPatients().replace("/"+idPat,""));
                caregiver1.setPatients(caregiver1.getPatients().replace(idPat,""));
            }
        }
        caregiverRepo.save(caregiver1);
    }

}
