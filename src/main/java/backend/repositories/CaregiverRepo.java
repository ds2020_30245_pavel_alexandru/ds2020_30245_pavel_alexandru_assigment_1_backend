package backend.repositories;

import backend.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public interface CaregiverRepo extends JpaRepository<Caregiver, UUID> {

    Optional<Caregiver> findById(UUID id);

    ArrayList<Caregiver> findByName(String name);

    ArrayList<Caregiver> findByDob(Date date);

    ArrayList<Caregiver> findByGender(String gender);

    ArrayList<Caregiver> findByAddress(String address);

    ArrayList<Caregiver> findByPatients(String patients);

    <S extends Caregiver> S save(S s);

    void deleteById(UUID id);

}
