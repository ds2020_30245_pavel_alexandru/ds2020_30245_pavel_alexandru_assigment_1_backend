package backend.dtos.builders;

import backend.dtos.UserDTO;
import backend.entities.Users;

public class UserBuilder {

    public static UserDTO toUserDTO(Users user) {
        return new UserDTO(user.getId(),user.getLinkId(),user.getUserName(), user.getPassword(),user.getUsertype());
    }

    public static Users toEntity(UserDTO user)
    {
        return new Users(user.getLinkId(),user.getUserName(),user.getPassword(),user.getUsertype());
    }
}
