package backend.dtos.builders;

import backend.dtos.MedicationPlanDTO;
import backend.entities.MedicationPlan;

public class MedicationPlanBuilder {

    public static MedicationPlanDTO medicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(),medicationPlan.getPatientId(),medicationPlan.getMeds(),medicationPlan.getMedsTime(),medicationPlan.getStartDate(),medicationPlan.getEndDate());
    }

    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO)
    {
        return new MedicationPlan(medicationPlanDTO.getPatientId(),medicationPlanDTO.getMeds(),medicationPlanDTO.getMedsTime(),medicationPlanDTO.getStartDate(),medicationPlanDTO.getEndDate());
    }
}
