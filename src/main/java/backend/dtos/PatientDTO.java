package backend.dtos;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class PatientDTO {

    private UUID id;
    private String name;
    private Date dob;
    private String gender;
    private String address;
    private String medRecord;

    public PatientDTO (UUID id, String name, Date dob, String gender, String address, String medRecord)
    {
        this.id=id;
        this.name=name;
        this.dob=dob;
        this.gender=gender;
        this.address=address;
        this.medRecord=medRecord;
    }

    public UUID getId(){return id;}

    public void setId(UUID id){this.id=id;}

    public String getName(){return name;}

    public void setName(String name){this.name=name;}

    public Date getDob(){return  dob; }

    public void setDob(Date dob){this.dob=dob;}

    public String getGender(){return gender;}

    public void setGender(String gender){this.gender=gender;}

    public String getAddress(){return address;}

    public void setAddress(String address){this.address=address;}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return Objects.equals(id, patientDTO.id) &&
                Objects.equals(name, patientDTO.name) &&
                Objects.equals(dob, patientDTO.dob) &&
                Objects.equals(gender, patientDTO.gender) &&
                Objects.equals(address, patientDTO.address) &&
                Objects.equals(medRecord, patientDTO.medRecord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name,dob,gender,address,medRecord);
    }

    public String getMedRecord() {
        return medRecord;
    }

    public void setMedRecord(String medRecord) {
        this.medRecord = medRecord;
    }
}
